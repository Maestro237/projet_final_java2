package doc.biblio.badeDeDonnee;

import javax.swing.*;
import java.sql.*;

public class GestionnaireBasedeDonnee {

    private static GestionnaireBasedeDonnee handler;

    private static final String DB_URL = "jdbc:derby:database;create=true";
    private static Connection conn = null;
    private static Statement requete = null;

    public GestionnaireBasedeDonnee(){
        createConnection();
        tableOeuvre();
        tableAdherent();
    }

    void createConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            conn = DriverManager.getConnection(DB_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void tableOeuvre(){
        String NOM_TABLE = "OEUVRE";
        try {
            requete = conn.createStatement();
            DatabaseMetaData dbm = conn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, NOM_TABLE.toUpperCase(), null);
            if (tables.next()){
                System.out.println("La table " + NOM_TABLE + " existe déjà. Allons y!");
            } else {
                requete.execute("CREATE TABLE " + NOM_TABLE + " (ID INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), TITRE VARCHAR(200), AUTEUR VARCHAR(200), EXEMPLAIRE INT DEFAULT 0, DISPONIBLE BOOLEAN DEFAULT TRUE)");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage() + " .... GestionBaseDonnée");
        }
    }

    void tableAdherent(){
        String NOM_TABLE = "ADHERENT";
        try {
            requete = conn.createStatement();
            DatabaseMetaData dbm = conn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, NOM_TABLE.toUpperCase(), null);
            if (tables.next()){
                System.out.println("La table " + NOM_TABLE + " existe déjà. Allons y!");
            } else {
                requete.execute("CREATE TABLE " + NOM_TABLE + " (ID INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(200), PRENOM VARCHAR(200), ADRESSE VARCHAR(200))");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage() + " .... GestionBaseDonnée");
        }
    }

    public ResultSet executeRequete(String requete1) {
        ResultSet resultat;
        try {
            requete = conn.createStatement();
            resultat = requete.executeQuery(requete1);
        } catch (SQLException e) {
            System.out.println("Generation d'Exception execQuery:GestionnaireBaseDonnee " + e.getLocalizedMessage());
            return null;
        }
        return resultat;
    }

    public boolean actionRequeteBase(String requete2){
        try{
            requete = conn.createStatement();
            requete.execute(requete2);
            return true;
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Erreur: " + e.getMessage(), "Pressence d'Erreur", JOptionPane.ERROR_MESSAGE);
            System.out.println("Generation d'Exception execQuery:GestionnaireBaseDonnee " + e.getLocalizedMessage());
            return false;
        }
    }


}
