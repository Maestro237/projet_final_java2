package doc.biblio.ui.ajoutAdherent;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EnregistreAdherent extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("ajoutAdherent.fxml"));
        primaryStage.setTitle("Ajout Adherent");
        primaryStage.setScene(new Scene(root));
        primaryStage.getIcons().add(new Image("file:icons8-library-50.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
