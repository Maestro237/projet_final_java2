package doc.biblio.ui.ajoutAdherent;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import doc.biblio.badeDeDonnee.GestionnaireBasedeDonnee;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import java.net.URL;
import java.util.ResourceBundle;

public class AjoutAdherentController implements Initializable {
    GestionnaireBasedeDonnee gestion ;

    public JFXTextField name;
    public JFXTextField surname;
    public JFXTextField address;
    public JFXButton enregistrer;
    public JFXButton annuler;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gestion = new GestionnaireBasedeDonnee();

    }

    public void ajoutMembre(ActionEvent actionEvent) {
        String nomAdherent = name.getText();
        String prenomAdherent = surname.getText();
        String adresseAdherent = address.getText();

        Boolean champVide = nomAdherent.isEmpty() ||prenomAdherent.isEmpty() || adresseAdherent.isEmpty();

        if (champVide == true){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Tout les champs sont requis");
            alert.showAndWait();
            return;
        }
        String requete =  "INSERT INTO ADHERENT (NOM, PRENOM, ADRESSE) VALUES ("+
                            "'" + nomAdherent + "'," +
                            "'" + prenomAdherent + "'," +
                            "'" + adresseAdherent + "'" +
                            ")";
        System.out.println(requete);
        if(gestion.actionRequeteBase(requete)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Enregistrement Effectuer !");
            alert.showAndWait();
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Erreur d'Enregistrement");
            alert.showAndWait();
        }

    }

    public void annulerEnregistre(ActionEvent actionEvent) {
    }
}
