package doc.biblio.ui.listeLivre;

import doc.biblio.badeDeDonnee.GestionnaireBasedeDonnee;
import doc.biblio.ui.ajoutOeuvre.DocumentFXMLController;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListeOeuvreController implements Initializable {

    public TableView vueTable;
    ObservableList<Oeuvre> liste = FXCollections.observableArrayList();

    public TableColumn<Oeuvre, Integer> colID;
    public TableColumn<Oeuvre, String> colTitre;
    public TableColumn<Oeuvre, String> colAuteur;
    public TableColumn<Oeuvre, Integer> colExemplaire;
    public TableColumn<Oeuvre, Boolean> colDispo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initValeurCol();
        GestionnaireBasedeDonnee gestion = new GestionnaireBasedeDonnee();
        chargementDonnees();
    }

    private void chargementDonnees() {
        GestionnaireBasedeDonnee gestion = new GestionnaireBasedeDonnee();
        String requete = "SELECT * FROM OEUVRE";
        ResultSet resultat = gestion.executeRequete(requete);
        try{
            while(resultat.next()){
                int id = resultat.getInt("ID");
                String titre = resultat.getString("TITRE");
                String auteur = resultat.getString("AUTEUR");
                int nExemplaire = resultat.getInt("EXEMPLAIRE");
                boolean dispo = resultat.getBoolean("DISPONIBLE");

                liste.add(new Oeuvre(id, titre, auteur, nExemplaire, dispo));

            }
        }catch (SQLException e){
            Logger.getLogger(DocumentFXMLController.class.getName()).log(Level.SEVERE, null, e);
        }

        vueTable.getItems().setAll(liste);


    }

    private void initValeurCol() {
        colID.setCellValueFactory(new PropertyValueFactory<>("Id"));
        colTitre.setCellValueFactory(new PropertyValueFactory<>("Titre"));
        colAuteur.setCellValueFactory(new PropertyValueFactory<>("Auteur"));
        colExemplaire.setCellValueFactory(new PropertyValueFactory<>("Exemplaires"));
        colDispo.setCellValueFactory(new PropertyValueFactory<>("Disponibilite"));
    }

    public static class Oeuvre {
        private final SimpleIntegerProperty id;
        private final SimpleStringProperty titre;
        private final SimpleStringProperty auteur;
        private final SimpleIntegerProperty exemplaires;
        private final SimpleBooleanProperty disponibilite;

        public Oeuvre(int id, String titre, String auteur, int exemplaires, Boolean disponibilite) {
            this.id = new SimpleIntegerProperty(id);
            this.titre = new SimpleStringProperty(titre);
            this.auteur = new SimpleStringProperty(auteur);
            this.exemplaires = new SimpleIntegerProperty(exemplaires);
            this.disponibilite = new SimpleBooleanProperty(disponibilite);
        }

        public int getId() {
            return id.get();
        }

        public SimpleIntegerProperty idProperty() {
            return id;
        }

        public String getTitre() {
            return titre.get();
        }

        public SimpleStringProperty titreProperty() {
            return titre;
        }

        public String getAuteur() {
            return auteur.get();
        }

        public SimpleStringProperty auteurProperty() {
            return auteur;
        }

        public int getExemplaires() {
            return exemplaires.get();
        }

        public SimpleIntegerProperty exemplairesProperty() {
            return exemplaires;
        }

        public boolean getDisponibilite() {
            return disponibilite.get();
        }

        public SimpleBooleanProperty disponibiliteProperty() {
            return disponibilite;
        }

        public void setId(int id) {
            this.id.set(id);
        }

        public void setTitre(String titre) {
            this.titre.set(titre);
        }

        public void setAuteur(String auteur) {
            this.auteur.set(auteur);
        }

        public void setExemplaires(int exemplaires) {
            this.exemplaires.set(exemplaires);
        }

        public void setDisponibilite(boolean disponibilite) {
            this.disponibilite.set(disponibilite);
        }
    }
}
