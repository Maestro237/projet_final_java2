package doc.biblio.ui.ajoutOeuvre;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("DocumentFXML.fxml"));
        primaryStage.setTitle("Ajout Livre");
        primaryStage.setScene(new Scene(root));
        primaryStage.getIcons().add(new Image("file:icons8-library-50.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
