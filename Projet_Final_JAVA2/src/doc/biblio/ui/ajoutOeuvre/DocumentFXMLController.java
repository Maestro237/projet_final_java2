package doc.biblio.ui.ajoutOeuvre;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import doc.biblio.badeDeDonnee.GestionnaireBasedeDonnee;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DocumentFXMLController implements Initializable {

    public JFXButton enregistrer;
    public JFXTextField titre;
    public JFXTextField auteur;
    public JFXComboBox nExemplaire;
    public AnchorPane uiRacine;
    ComboBox<Object> comboBox;

    GestionnaireBasedeDonnee gestionnaireBasedeDonnee;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gestionnaireBasedeDonnee = new GestionnaireBasedeDonnee();
        verificationDesDonnees();
    }

    private void verificationDesDonnees() {
        String requete = "SELECT TITRE FROM OEUVRE";
        ResultSet resultat = gestionnaireBasedeDonnee.executeRequete(requete);
        try {
            while (resultat.next()){
                String titre = resultat.getString("TITRE");
                System.out.println(titre);
            }
        } catch (SQLException e) {
            Logger.getLogger(DocumentFXMLController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void ajoutOeuvre(ActionEvent actionEvent) {
        String titreOeuvre = titre.getText();
        String auteurOeuvre = auteur.getText();
        int nombreExemplaire = (int) nExemplaire.getValue();

        if(titreOeuvre.isEmpty() || auteurOeuvre.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Tout les champs sont requis");
            alert.showAndWait();
            return;
        }

      /*  requete.execute("CREATE TABLE " + NOM_TABLE + "("
                + " id INT(45) PRIMARY KEY GENERATED ALWAYS AS IDENTITY,\n"
                + " titre VARCHAR(200),\n"
                + " auteur VARCHAR(200),\n"
                + " nExemplaire INT(45),\n"
                + " estDisponible BOOLEAN DEFAULT TRUE"
                + " )");*/

        String requete = "INSERT INTO OEUVRE (TITRE, AUTEUR, EXEMPLAIRE, DISPONIBLE) VALUES ("+
                        "'" + titreOeuvre + "'," +
                        "'" + auteurOeuvre + "'," +
                        "" + nombreExemplaire + "  ,"+
                        "" + true +
                        ")";
        System.out.println(requete);
        if (gestionnaireBasedeDonnee.actionRequeteBase(requete)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Instertion Success!");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Erreur Insertion");
            alert.showAndWait();
        }
    }

    public void annulerEnregistre(ActionEvent actionEvent) {

        Stage stg = (Stage)uiRacine.getScene().getWindow();
        stg.close();
    }
}
