package com.ulrich.projet;

import java.time.LocalDate;
import java.util.Hashtable;
import java.util.Map;

public class Adherent {

    /**
     * Declaring private variables of class Adherent
     */
    private String nom;
    private String prenom;
    private String adresse;

    /**
     * Link between Adherent and Exemplaire to all items borrowed by someone
     */
    Map<Exemplaire, Pret> dicoExemplairePret;

    public Adherent(String nom, String prenom, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        dicoExemplairePret = new Hashtable<>();
    }

    public Adherent(){}


    /**
     * Generating getters and setters
     */

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Map<Exemplaire, Pret> getDicoExemplairePret() {
        return dicoExemplairePret;
    }

    public void setDicoExemplairePret(Map<Exemplaire, Pret> dicoExemplairePret) {
        this.dicoExemplairePret = dicoExemplairePret;
    }

    /**
     * Declaring the methods of the class
     */

    public Exemplaire emprunter(Oeuvre oeuvre){
        //Retrieving today's date and setting it up to debut and fin
        Exemplaire nouvelleExemplaire = null;

        if (oeuvre.getNombreExemplaire() > 0){
            LocalDate dateActuel = LocalDate.now();
            String numeroExemplaire = Exemplaire.genereNumero_Oeuvre();
            nouvelleExemplaire  = new Exemplaire(numeroExemplaire, oeuvre);

            //Initiating Borrowing
            Pret nouveauPret = new Pret();
            nouveauPret.setNumero(Pret.genereNumero_Pret());
            nouveauPret.setDebut(dateActuel);
            nouveauPret.setFin(dateActuel.plus(Pret.getPeriodePret()));

            //Reducing nombreExemplaire of Oeuvre
            oeuvre.setNombreExemplaire(oeuvre.getNombreExemplaire() - 1);

            //Adding the book to the user's history
            dicoExemplairePret.put(nouvelleExemplaire, nouveauPret);
        }

        //A REVOIR
        return nouvelleExemplaire;
    }

    public void ramener(Exemplaire exemplaire){
        if (dicoExemplairePret.isEmpty())
            System.out.println("Vous n'avez effectuer aucun prêt !");
        else if (dicoExemplairePret.containsKey(exemplaire)){
            System.out.println("===Informations livre rendu===\n" );
            System.out.println("Annulation des prêts....");
            dicoExemplairePret.remove(exemplaire);

            //Mise a jour des Oeuvre
            exemplaire.ramener();

            System.out.println("Consultations des prêts....");
            System.out.println(onAfficheLesObjetsPretsDansLesMaps(this.dicoExemplairePret) + "\n");
        }
    }

    public String toString(){
        return "\nIdentifiants Adhérent : \n" +
                "== Nom adhérent : " + this.nom + "\n" +
                "== Prénom adhérent : " + this.prenom + "\n" +
                "== Adresse adhérent : " + this.adresse + "\n" +
                "===Informations concernant le prêt===" + "\n" +
                onAfficheLesObjetsPretsDansLesMaps(this.dicoExemplairePret) + "\n";
    }

    public String onAfficheLesObjetsPretsDansLesMaps(Map<Exemplaire, Pret> monMap){
        String contenu = "";
        if (!(monMap.isEmpty())) {
            for (Map.Entry<Exemplaire, Pret> donnee : monMap.entrySet())
                contenu = "Numéro exemplaire : " + donnee.getKey() + "\n" + donnee.getValue().toString();
            return contenu;
        } else {
            return "Vous n'avez aucun prêt en cours...";
        }
    }


}
