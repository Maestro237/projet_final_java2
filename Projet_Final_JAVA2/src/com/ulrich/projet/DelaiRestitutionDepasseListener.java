package com.ulrich.projet;

import java.util.EventListener;

public interface DelaiRestitutionDepasseListener extends EventListener {
    public void exmplaireNonRestitue(DelaiDepasseEvent evt);
}
