package com.ulrich.projet;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;

public class Exemplaire {

    /**
     * Declaring private variables of class Exemplaire
     */
    private String numero;

    private Map<String, Oeuvre> exmpl;

    public Exemplaire(String numero) {
        this.numero = numero;
    }

    public Exemplaire(String numero,  Oeuvre exmpl) {
        this.numero = numero;
        this.exmpl = new Hashtable<>();
        this.exmpl.put(numero, exmpl);
    }

    public Exemplaire(){}


    /**
     * Generating getters and setters
     */
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Declaring the methods of the class
     */
    public void ramener(){

        this.exmpl.get(this.getNumero()).setNombreExemplaire(this.exmpl.get(this.getNumero()).getNombreExemplaire() + 1);

    }

    public static String genereNumero_Oeuvre(){

        SecureRandom numeroRandom = new SecureRandom();
        byte bytes[] = new byte[5];
        numeroRandom.nextBytes(bytes);
        Base64.Encoder crypt = Base64.getUrlEncoder().withoutPadding();
        String token = crypt.encodeToString(bytes);
        return token ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exemplaire that = (Exemplaire) o;
        return numero.equals(that.numero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero);
    }
}
