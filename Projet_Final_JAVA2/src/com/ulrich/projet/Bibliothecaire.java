package com.ulrich.projet;

public class Bibliothecaire implements DelaiRestitutionDepasseListener{


    @Override
    public void exmplaireNonRestitue(DelaiDepasseEvent evt) {
        System.out.println("\n Nom Adherent : " + evt.getAdherent().getNom() + "\n"
                            + " Prenom Adherent : " + evt.getAdherent().getPrenom() + "\n"
                            + " Numéro Exemplaire à rendre : " + evt.getExplaire().getNumero());

    }
}
