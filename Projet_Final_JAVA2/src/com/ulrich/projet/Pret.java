package com.ulrich.projet;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.Period;
import java.util.Base64;

public class Pret {

    /**
     * Declaring private variables of class Pret
     */
    private static final Period periodePret = Period.ofDays(0);
    private String numero;
    private LocalDate debut;
    private LocalDate fin;

    public static Period getPeriodePret() {
        return periodePret;
    }

    /**
     * Generating getters and setters
     */
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDebut() {
        return debut;
    }

    public void setDebut(LocalDate debut) {
        this.debut = debut;
    }

    public LocalDate getFin() {
        return fin;
    }

    public void setFin(LocalDate fin) {
        this.fin = fin;
    }

    public static String genereNumero_Pret(){

        SecureRandom numeroRandom = new SecureRandom();
        byte bytes[] = new byte[3];
        numeroRandom.nextBytes(bytes);
        Base64.Encoder crypt = Base64.getUrlEncoder().withoutPadding();
        String token = crypt.encodeToString(bytes);
        return token ;
    }

    @Override
    public String toString() {
        return  "Numéro de prêt " + "-> " + this.numero + "\n" +
                "Date de début de prêt " + "-> " + this.debut + "\n"+
                "Date de fin de prêt " + "-> " + this.fin + "\n";
    }
}
