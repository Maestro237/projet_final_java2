package com.ulrich.projet;

import java.util.EventObject;

public class DelaiDepasseEvent extends EventObject {

    private Exemplaire explaire;
    private Adherent adherent;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public DelaiDepasseEvent(Object source) {
        super(source);
    }

    public Exemplaire getExplaire() {
        return explaire;
    }

    public void setExplaire(Exemplaire explaire) {
        this.explaire = explaire;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    public DelaiDepasseEvent(Object source, Exemplaire explaire, Adherent adherent) {
        super(source);
        this.explaire = explaire;
        this.adherent = adherent;
    }
}
