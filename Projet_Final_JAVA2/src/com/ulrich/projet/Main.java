package com.ulrich.projet;

public class Main {

    public static void main(String[] args) {

        try {

            Adherents adherents = new Adherents();
            adherents.restaurer();
            Bibliothecaire bibliothecaire = new Bibliothecaire();
            adherents.addDelaiRestitutionDepasseListener(bibliothecaire);
            Thread t1 = new Thread(adherents);

            Adherent adherent1 = new Adherent( "Albert", "Durant", "2 allee tataouine");
            adherents.addAdherent(adherent1);
            Adherent adherent2 = new Adherent( "Ulrich", "Durant 1", "2 Roses tataouine");
            adherents.addAdherent(adherent2);

            String titre = "BestOfLouisMariano" ;
            String auteur = "Louis Mariano" ;
            int nombreExemplaire = 3 ;
            Varietee varietee1 = new Varietee(titre, auteur, nombreExemplaire);

            String titre1 = "BestOfLouis" ;
            String auteur1 = "Mariano" ;
            int nombreExemplaire1 = 3 ;
            Opera opera1 = new Opera(titre1, auteur1, nombreExemplaire1);


            System.out.println("\n*********AVANT PRET*********\n");
            System.out.println("===Adherent 1 avant emprunt=== " + adherent1);


            Exemplaire exemplaire = adherent1.emprunter(varietee1);
            Exemplaire exemplaire1 = adherent2.emprunter(opera1);
            System.out.println("\n*********APRES PRET*********\n");
            System.out.println( "Adherent 1 apres emprunt = " + adherent1 ) ;


            //adherent1.ramener(exemplaire) ;
            System.out.println("\n*********APRES RESTITUTION*********\n");
            System.out.println("===Adherent 1 après restitution=== " + adherent1);

            /*** DIAGRAMME DE CLASSE V2 ET GESTION DES OBJETS ***/

            System.out.println("\nDIAGRAMME DE CLASSE V2 ET GESTION DES OBJETS\n");

            /*** Gestion de la Persistance Objet ***/

            System.out.println("\nGestion de la Persistance Objet\n");
            t1.start();


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
