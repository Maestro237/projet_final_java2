package com.ulrich.projet;

public class Opera extends Oeuvre {

    /**
     * Constructor
     *
     * @param titre
     * @param auteur
     * @param nombreExemplaire
     */

    private Integer nombreExemplaire;
    public Opera(String titre, String auteur, Integer nombreExemplaire) {
        super(titre, auteur, nombreExemplaire);
    }

    @Override
    public String toString() {
        return  "Opéra: " + super.toString();
    }
}
