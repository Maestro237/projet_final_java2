package com.ulrich.projet;

public class Oeuvre {

    /**
     * Declaring private variables of class Oeuvre
     */
    private int id;
    private String titre;
    private String auteur;
    private Integer nombreExemplaire;

    /**
     * Constructor
     */
    public Oeuvre(String titre, String auteur, Integer nombreExemplaire) {
        this.titre = titre;
        this.auteur = auteur;
        this.nombreExemplaire = nombreExemplaire;
    }

    /**
     * Generating getters and setters
     */

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Integer getNombreExemplaire() {
        return nombreExemplaire;
    }

    public void setNombreExemplaire(Integer nombreExemplaire) {
        this.nombreExemplaire = nombreExemplaire;
    }

    @Override
    public String toString() {
        return  "Nom de l'oeuvre " + "-> " + this.titre + "\n" +
                "Nom de l'auteur " + "-> " + this.auteur + "\n";
    }
}
