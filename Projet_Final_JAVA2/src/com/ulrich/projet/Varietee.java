package com.ulrich.projet;

public class Varietee extends Oeuvre {

    /**
     * Constructor
     *
     * @param titre
     * @param auteur
     * @param nombreExemplaire
     */

    /*private Integer nombreExemplaire;

    public Integer getNombreExemplaire() {
        return nombreExemplaire;
    }*/

    /*public void setNombreExemplaire(Integer nombreExemplaire) {
        this.nombreExemplaire = nombreExemplaire;
    }*/

    public Varietee(String titre, String auteur, Integer nombreExemplaire) {
        super(titre, auteur, nombreExemplaire);
        //this.nombreExemplaire = nombreExemplaire;
    }

    @Override
    public String toString() {
        return  "Varietée: " + super.toString();
    }
}
