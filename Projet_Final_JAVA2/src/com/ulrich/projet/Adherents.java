package com.ulrich.projet;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Adherents implements Runnable {
    private List<DelaiRestitutionDepasseListener> listenerList = new ArrayList<>();
    private List<Adherent> adherentList = new ArrayList<>();

    @Override
    public void run() {

        while (true){
            try {
                exmplaireNonRestitue();
                Thread.sleep(5000);
                this.sauvegarder();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void exmplaireNonRestitue(){
        //ArrayList<DelaiDepasseEvent> result = new ArrayList<DelaiDepasseEvent>();
        for (Adherent adherent: adherentList) {
            for (Pret pret:adherent.dicoExemplairePret.values()) {
                //if (pret.getDebut().plus(Pret.getPeriodePret()).isAfter(LocalDate.now().minus(Pret.getPeriodePret())))
                if (LocalDate.now().isAfter(pret.getDebut().plus(Pret.getPeriodePret())))
                    System.out.println("oui oui");
                    final DelaiDepasseEvent delaiDepasseEvent = new DelaiDepasseEvent(this, adherent.dicoExemplairePret.entrySet().stream().filter(e -> e.getValue().equals(pret)).map(Map.Entry::getKey).findFirst().orElseThrow(RuntimeException::new), adherent);
                    for (DelaiRestitutionDepasseListener dl:listenerList) {
                          dl.exmplaireNonRestitue(delaiDepasseEvent);
                    }
            }
        }

        /*for (DelaiDepasseEvent dde:result) {
            for (DelaiRestitutionDepasseListener dl:listenerList) {
                dl.exmplaireNonRestitue(dde);
            }
        }*/
    }

    public void addAdherent(Adherent adherent2) {
        adherentList.add(adherent2);
    }

    public void addDelaiRestitutionDepasseListener(DelaiRestitutionDepasseListener dRdL) {
        listenerList.add(dRdL);
    }

    public void sauvegarder() {
        try {
            XMLEncoder sauvegarde = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("D:\\Java Projects\\Projet_Final_JAVA2\\sauvegarde.xml")));
            for (Adherent adherent:adherentList) {
                sauvegarde.writeObject(adherent);
            }
            sauvegarde.flush();
            sauvegarde.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void restaurer() {
        try {
            XMLDecoder restaurer = new XMLDecoder(new BufferedInputStream(new FileInputStream("D:\\Java Projects\\Projet_Final_JAVA2\\sauvegarde.xml")));
            Adherent adherent = (Adherent)restaurer.readObject();
            restaurer.close();
            System.out.println("Restauration: \n Nom Adherent : " + adherent.getNom());
            System.out.println("Prenom Adherent : " + adherent.getPrenom());
            System.out.println("Adresse Adherent : " + adherent.getAdresse());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
